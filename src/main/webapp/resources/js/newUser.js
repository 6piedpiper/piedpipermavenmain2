	    
	  var rootUrl="http://localhost:8080/PiedPiperProject/rest/user/newUser";

	  
	  $(document).on("click", '#newUser', function(){addUser();})

	  var addUser=function(){
	  	console.log('addUser');
	  	$.ajax({
	  		type:'POST',
	  		contentType:'application/json',
	  		url:"http://localhost:8080/PiedPiperProject/rest/user/newUser",
	  		dataType:"json",
	  		data:formToJSON(),
	  		success: function(data, textStatus, jqXHR){
	  			clearAll();
	  			alert("User created successfully");
	  		},
	  		error: function(jqXHR,textStatus,errorThrown){
	  			alert('addUser error: '+textStatus);
	  		}
	  	});
	  }

	  var formToJSON=function(){
		  console.log('stringify');
	  	return JSON.stringify({
	  		"empId":$('#empId').val(),
	  		"userName":$('#userName').val(),
	  		"pWord":$('#pWord').val(),
	  		"userRole":$('#userRole').val()
	  	})
	  	
	  }
	  
	  var clearAll=function(){
		  console.log('clear')
			$('#empId').val("");
  			$('#userName').val("");
  			$('#pWord').val("");
  			$('#userRole').val("");
	  }