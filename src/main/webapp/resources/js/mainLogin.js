var rootUrl = "http://localhost:8090/PiedPieperProject/rest/user/login/";

var $ = function(id) {
	return document.getElementById(id);
};

function LogIn() {

	/* stop form from submitting normally */
	event.preventDefault();

	var str3 = document.getElementById("userName").value;
	var str4 = document.getElementById("password").value;

	if (str3 != "" && str4 != "") {
		console.log(document.getElementById("userName").value);
		console.log(document.getElementById("password").value);
		console.log(str3);
		console.log(str4);

		jQuery
				.ajax({
					type : 'POST',
					contentType : 'application/json',
					url : "http://localhost:8090/PiedPiperProject/rest/user/login/",
					dataType : "json",
					data : JSON.stringify({
						"username" : str3,
						"password" : str4
					}),

					success : function(resp, status) {
						if (resp === "http://localhost:8090/PiedPiperProject/index.html") {

							document.getElementById("error").innerHTML = "Failed Login Attempt";
						} else {
							window.location.href = resp;
						}
					},
					error : function() {
						alert('failed login: ');
					}
				});
	}
}
