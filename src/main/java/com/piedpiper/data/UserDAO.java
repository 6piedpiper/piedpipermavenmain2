package com.piedpiper.data;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.piedpiper.users.User;

@Stateless
@LocalBean
public class UserDAO {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		Query query = em.createQuery("SELECT u FROM User u");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsersByName(String userName) {
		Query query = em.createQuery("SELECT u FROM User AS u " + "WHERE u.userName LIKE ?1");
		query.setParameter(1, "%" + userName + "%");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public URL successfulLoginUrl(String userName, String pWord) throws MalformedURLException {
		String urlRoot = "http://localhost:8090/PiedPiperProject/dashboard-";
		URL successUrl = null;
		List<User> user = null;
		Query query = em.createQuery("SELECT u FROM User AS u WHERE u.userName LIKE ?1 AND u.pWord LIKE ?2");
		query.setParameter(1, userName);
		query.setParameter(2, pWord);
		try {

			user = (List<User>) query.getResultList();

			if (user.size() == 0) {
				urlRoot = "http://localhost:8090/PiedPiperProject/index.html";
				successUrl = new URL(urlRoot);
				return successUrl;
			}

			if (user.size() != 0) {
				String userRole = user.get(0).getUserRole().toString();
				System.out.println(userRole);
				System.out.println("User Role:: " + userRole);
				switch (userRole) {
				case "SYSTEM_ADMINISTRATOR":
					urlRoot += "Admin.html";
					break;
				case "CUSTOMER_SERVICE_REPRESENTATIVE":
					urlRoot += "CSR.html";
					break;
				case "SUPPORT_ENGINEER":
					urlRoot += "SEng.html";
					break;
				case "NETWORK_MANAGEMENT_ENGINEER":
					urlRoot += "NEng.html";
					break;

				default:
					System.out.println("....No User Role....");
					urlRoot = "http://localhost:8090/PiedPiperProject/index.html";
					break;
				}
			}
			successUrl = new URL(urlRoot);
			return successUrl;
		} catch (EJBTransactionRolledbackException e) {
			urlRoot = "http://localhost:8090/PiedPiperProject/index.html";
			successUrl = new URL(urlRoot);
			return successUrl;
		} catch (NullPointerException e) {
			urlRoot = "http://localhost:8090/PiedPiperProject/index.html";
			successUrl = new URL(urlRoot);
			return successUrl;
		}

	}

	public User getUserById(int empId) {
		return em.find(User.class, empId);
	}

	public void add(User user) {
		em.persist(user);
	}

	public void update(User user) {
		System.out.println("User with id: " + user.getEmpId() + " was updated");
		em.merge(user);
	}

	// pass userName
	public void delete(int empId) {
		System.out.println("User with id: " + empId + " was deleted");
		em.remove(getUserById(empId));
	}

	public void deleteTable() {
		em.createQuery("DELETE FROM (TableName)").executeUpdate();
	}

}
