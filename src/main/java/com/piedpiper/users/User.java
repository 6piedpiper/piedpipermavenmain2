package com.piedpiper.users;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class User {

	@Id
	private int empId;

	private String userName;

	private String pWord;

	@Enumerated(EnumType.STRING)
	private UserRole userRole;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getpWord() {
		return pWord;
	}

	public void setpWord(String pWord) {
		this.pWord = pWord;
	}

	public String getUserRole() {
		return userRole.toString();
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	@Override
	public String toString() {
		return "User [id= " + empId + ", User Name= " + userName + ", hashCode= " + hashCode() + "]";
	}

	@Override
	public int hashCode() {
		return this.empId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			if (this.empId == ((User) obj).empId) {
				return true;
			}
		}
		return false;
	}

}
