package com.piedpiper.dataimport;

import static com.piedpiper.dataimport.ExcelReader.readExcelFileToDB;

import java.io.IOException;
import java.io.InputStream;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
/**
 * @author James Runswick
 * @author Date Created 25 May 2016
 * @version 1.0
 *
 */
@WebServlet("/upload")
@MultipartConfig
// @Path("/upload")
// @Stateless
// @LocalBean
public class UploadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	@EJB
	private ImportExcelDAO sampleDatabaseAccess;

	// @POST
	// @Consumes({MediaType.MULTIPART_FORM_DATA})
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Part filePart = request.getPart("file"); 
		InputStream fileContent = filePart.getInputStream();
		readExcelFileToDB(fileContent,sampleDatabaseAccess);

	}

	

}
