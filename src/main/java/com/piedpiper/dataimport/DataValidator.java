package com.piedpiper.dataimport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.piedpiper.dataimport.tabObjects.BaseData;


public class DataValidator {
	
	static ImportExcelDAO importExcelDao;
	static DataValidator validator;
	int[] CAUSE_CODES;
	int[] MNCs;
	List<Integer[]> eventIdCauseCodes;
	List<Integer[]> mCCAndMNCCombs;
	List<Integer> uETypes;
	
	public static DataValidator getValidatorInstance(ImportExcelDAO excelDao){
		if(validator==null){
			validator = new DataValidator(excelDao);
			return validator;
		}else{
			return validator;
		}
	}
	
	private DataValidator(ImportExcelDAO excelDao){
		importExcelDao = excelDao;
		eventIdCauseCodes = importExcelDao.getEventIdCauseCodes();
		mCCAndMNCCombs = importExcelDao.getMCCAndMNCCombinations();
		uETypes = importExcelDao.getTACNumbers();
	}
	
	public boolean validateBaseDataObject(BaseData data){
		if(data.getDate() == null) return false;
		if(!validateEventIdAndCauseCode(data,eventIdCauseCodes)) return false;
		if(!validateMCCAndMNC(data, mCCAndMNCCombs)) return false;
		if(!validateUEType(data, uETypes)) return false;
		if(!validateFailureClass(data)) return false;
		if(!validateCellId(data)) return false;
		if(!validateHierIds(data)) return false;
		if(!validateDuration(data)) return false;
		if(!validateNEVersion(data)) return false;
		if(!validateIMSI(data)) return false;
		
		return true;
	}
	
	public boolean validateEventIdAndCauseCode(BaseData data, List<Integer[]> eventIdCauseCodes){
		int eventId = data.getEventId();
		int causeCode = data.getCauseCode();
		eventIdCauseCodes.add(new Integer[]{4099,0});
		for(Object[] rows:eventIdCauseCodes){
			if(eventId==(int)rows[0]&&causeCode==(int)rows[1]) return true;
		}
		return false;
	
	}
	
	public boolean validateMCCAndMNC(BaseData data, List<Integer[]> mCCAndMNCCombs){
		int mcc = data.getMarketNo();
		int mnc = data.getOperatorNo();
		for(Object[] rows:mCCAndMNCCombs){
			if(mcc==(int)rows[0]&&mnc==(int)rows[1]) return true;
		}
		return false;
	}
	
	public boolean validateFailureClass(BaseData data){
		int failureClass = data.getFailureClass();
		int[] classes = new int[]{0,1,2,3,4};
		for(int i:classes){
			if(i==failureClass) return true;
		}
		return false;
	}
	
	public boolean validateUEType(BaseData data, List<Integer> uETypes ){
		int uEType = data.getUEType();
		if(uETypes.contains(uEType)) return true;
		return false;
	}
	
//	public boolean validateCellIdAndHierId(BaseData data){
//		long[] hierIds = new long[]{4809532081614990000L,8226896360947470000L,1150444940909480000L};
//		cellIdHierCombo = new HashMap<Integer,long[]>();
//		cellIdHierCombo.put(4, hierIds);
//		hierIds = new long[]{7302598826786560000L,7901152815458020000L,550903905013920000L};
//		cellIdHierCombo.put(5, hierIds);
//		hierIds = new long[]{229001099943031000L,4970937722532610000L,6079299740152730000L};
//		cellIdHierCombo.put(3842, hierIds);
//		for(int cellId:cellIdHierCombo.keySet()){
//			if(data.getCellId()==cellId){
//				if(validateHierIds(data,cellIdHierCombo.get(cellId))) return true;
//			}
//		}
//		return false;
//	}
	
	public boolean validateCellId(BaseData data){
		int cellId = data.getCellId();
		if(cellId!=0) return true;
		return false;
	}
	
	
	public boolean validateHierIds(BaseData data){
		long hierId3 = data.getHIER3_ID();
		long hierId32 = data.getHIER32_ID();
		long hierId321 = data.getHIER321_ID();
		if(hierId3==0||String.valueOf(hierId3).length()>19) return false;
		if(hierId32==0||String.valueOf(hierId32).length()>19) return false;
		if(hierId321==0||String.valueOf(hierId321).length()>19) return false;
		return true;
	}
	
	public boolean validateDuration(BaseData data){
		int duration = data.getDuration();
		if(duration==1000) return true;
		return false;
	}
	
	public boolean validateNEVersion(BaseData data){
		String neVersion = data.getnEVersion();
		if(neVersion.matches("^[0-9]{2}[A-Ba-b]{1}$")&&neVersion.length()==3){
			return true;
		}
		return false;
	}
	
	public boolean validateIMSI(BaseData data){
		long imsi = data.getIMSO();
		if(String.valueOf(imsi).length()==15) return true;
		return false;
	}
}
