package com.piedpiper.dataimport;

import java.text.ParseException;

import org.apache.poi.ss.usermodel.Row;

/**
 * @author James Runswick
 * @author Date Created 23 May 2016
 * @version 1.0
 *
 */
public interface ExcelTabDataObject {
	void RetrieveObjectFromExcel(Row row) throws ParseException;

}
