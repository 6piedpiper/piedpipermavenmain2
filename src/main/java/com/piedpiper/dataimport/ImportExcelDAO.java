package com.piedpiper.dataimport;

import java.util.Collection;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.dataimport.tabObjects.UserEntity;

/**
 * @author James Runswick
 * @author Date Created 24 May 2016
 * @version 1.0
 *
 */
@Stateless
@LocalBean
public class ImportExcelDAO {

	@PersistenceContext//(unitName = "SampleData")
	private EntityManager em;

	public void addListOfEntities(Collection<ExcelTabDataObject> newObjects) {
		for (ExcelTabDataObject newObject : newObjects) {
			em.merge(newObject);
		}
	}

	public void addSingleEntity(ExcelTabDataObject newObject) {
		if (newObject instanceof BaseData)
			em.persist(newObject);
		em.merge(newObject);
	}

	public List<Integer[]> getEventIdCauseCodes() {
		Query query = em.createQuery("SELECT DISTINCT e.eventId, e.causeCode FROM event_cause e");
		return query.getResultList();
	}

	// public List<Integer> getFailureClasses(){ Will use if necessary..
	// Query query = em.createQuery("SELECT f.failureClass FROM failure_class
	// f");
	// return query.getResultList();
	// }

	public List<Integer[]> getMCCAndMNCCombinations() {
		Query query = em.createQuery("SELECT DISTINCT c.mmc, c.mnc FROM machine_code c");
		return query.getResultList();
	}

	public List<Integer> getTACNumbers() {
		Query query = em.createQuery("SELECT u.TAC FROM user_entity u");
		return query.getResultList();
	}
}
