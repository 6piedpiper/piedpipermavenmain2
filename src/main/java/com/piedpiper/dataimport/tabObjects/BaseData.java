package com.piedpiper.dataimport.tabObjects;

import static com.piedpiper.dataimport.ExcelReader.getIntValues;
import static com.piedpiper.dataimport.ExcelReader.getLongValue;
import static com.piedpiper.dataimport.ExcelReader.getStringValues;

import java.sql.Timestamp;
import java.text.ParseException;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.piedpiper.dataimport.ExcelTabDataObject;

/**
 * @author James Runswick
 * @author Date Created 23 May 2016
 * @version 1.0
 *
 */
@Entity(name = "BaseData")
public class BaseData implements ExcelTabDataObject {

	public BaseData() {

	}

	@Id
	private int recordId;
	private Timestamp failureDate;
	private int eventId;
	private int failureClass;
	private int UEType;
	private int marketNo;
	private int operatorNo;
	private int cellId;
	private int duration;
	private int causeCode;
	private String nEVersion;
	private long IMSO;
	private long HIER3_ID;
	private long HIER32_ID;
	private long HIER321_ID;

	/**
	 * Scans through the Row and sets each field based on column number
	 */
	@Override
	public void RetrieveObjectFromExcel(Row row) throws ParseException {
		for (Cell cell : row) {
			switch (cell.getColumnIndex()) {
			case 0:
				this.failureDate = new Timestamp(cell.getDateCellValue().getTime());
				break;
			case 1:
				this.eventId = getIntValues(cell);
			case 2:
				this.failureClass = getIntValues(cell);
			case 3:
				this.UEType = getIntValues(cell);
				break;
			case 4:
				this.marketNo = getIntValues(cell);
				break;
			case 5:
				this.operatorNo = getIntValues(cell);
				break;
			case 6:
				this.cellId = getIntValues(cell);
				break;
			case 7:
				this.duration = getIntValues(cell);
				break;
			case 8:
				this.causeCode = getIntValues(cell);
				break;
			case 9:
				this.nEVersion = getStringValues(cell);
				break;
			case 10:
				this.IMSO = getLongValue(cell);
			case 11:
				this.HIER3_ID = getLongValue(cell);
				break;
			case 12:
				this.HIER32_ID = getLongValue(cell);
				break;
			case 13:
				this.HIER321_ID = getLongValue(cell);
				break;
			default:
				break;
			}// end switch
		}
	}

	// @OneToOne
	public Timestamp getDate() {
		return failureDate;
	}

	public void setDate(Timestamp date) {
		this.failureDate = date;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getFailureClass() {
		return failureClass;
	}

	public void setFailureClass(int failureClass) {
		this.failureClass = failureClass;
	}

	public int getUEType() {
		return UEType;
	}

	public void setUEType(int uEType) {
		UEType = uEType;
	}

	public int getMarketNo() {
		return marketNo;
	}

	public void setMarketNo(int marketNo) {
		this.marketNo = marketNo;
	}

	public int getOperatorNo() {
		return operatorNo;
	}

	public void setOperatorNo(int operatorNo) {
		this.operatorNo = operatorNo;
	}

	public int getCellId() {
		return cellId;
	}

	public void setCellId(int cellId) {
		this.cellId = cellId;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getCauseCode() {
		return causeCode;
	}

	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}

	public String getnEVersion() {
		return nEVersion;
	}

	public void setnEVersion(String nEVersion) {
		this.nEVersion = nEVersion;
	}

	public long getIMSO() {
		return IMSO;
	}

	public void setIMSO(long iMSO) {
		IMSO = iMSO;
	}

	public long getHIER3_ID() {
		return HIER3_ID;
	}

	public void setHIER3_ID(long hIER3_ID) {
		HIER3_ID = hIER3_ID;
	}

	public long getHIER32_ID() {
		return HIER32_ID;
	}

	public void setHIER32_ID(long hIER32_ID) {
		HIER32_ID = hIER32_ID;
	}

	public long getHIER321_ID() {
		return HIER321_ID;
	}

	public void setHIER321_ID(long hIER321_ID) {
		HIER321_ID = hIER321_ID;
	}

	public int getRecordId() {
		return recordId;
	}

	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	public Timestamp getFailureDate() {
		return failureDate;
	}

	public void setFailureDate(Timestamp failureDate) {
		this.failureDate = failureDate;
	}

}