package com.piedpiper.dataimport.tabObjects;

import static com.piedpiper.dataimport.ExcelReader.getIntValues;
import static com.piedpiper.dataimport.ExcelReader.getStringValues;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.piedpiper.dataimport.ExcelTabDataObject;
/**
 * @author James Runswick
 * @author Date Created 23 May 2016
 * @version 1.0
 *
 */
@IdClass(MachinePK.class)
@Entity(name = "machine_code")
public class MachineCodeData implements ExcelTabDataObject {
	
	@Id
	private int mmc;
	@Id
	private int mnc;
	private String country;
	private String operator;
	@Override
	public void RetrieveObjectFromExcel(Row row) {
		for (Cell cell: row){
			switch (cell.getColumnIndex()) {
			case 0:
				this.mmc = getIntValues(cell);
				break;
			case 1:
				this.mnc = getIntValues(cell);
				break;
			case 2:
				this.country = getStringValues(cell);
				break;
			case 3:
				this.operator = getStringValues(cell);
				break;
			}
		}
	}
	/**
	 * @return the mmc
	 */
	public int getMmc() {
		return mmc;
	}
	/**
	 * @param mmc the mmc to set
	 */
	public void setMmc(int mmc) {
		this.mmc = mmc;
	}
	/**
	 * @return the mnc
	 */
	public int getMnc() {
		return mnc;
	}
	/**
	 * @param mnc the mnc to set
	 */
	public void setMnc(int mnc) {
		this.mnc = mnc;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}
	/**
	 * @param operator the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

}
