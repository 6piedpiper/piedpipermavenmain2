package com.piedpiper.dataimport;

/**
 * @author James Runswick
 * @author Date Created 27 May 2016
 * @version 1.0
 *
 */
public enum FailureClass {

	EMERGENCY(0,"EMERGENCY"),
	HIGH_PRIORITY_ACCESS(1,"HIGH PRIORITY ACCESS"),
	MT_ACCESS(2,"MT ACCESS"),
	MO_SIGNALLING(3, "MO SIGNALLING"),
	MO_DATA(4, "MO DATA");
	
	private int tableValue;
	private String tableStringEntry;
	
	private FailureClass(int tableValue, String tableStringEntry){
		this.tableValue = tableValue;
		this.tableStringEntry = tableStringEntry;
	}

	public int getTableValue() {
		return tableValue;
	}

	public String getTableStringEntry() {
		return tableStringEntry;
	}
	
	@Override
	public String toString(){
		return tableStringEntry;
	}
	
	
	
	
}
