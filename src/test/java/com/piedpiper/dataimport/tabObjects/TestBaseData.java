package com.piedpiper.dataimport.tabObjects;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

public class TestBaseData extends EntityTest {
	
	private BaseData testData=new BaseData();
	private DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	@Override
	void fillValidRow() {
		try {
			validRow.createCell(0).setCellValue(new Timestamp(df.parse("11/01/2013 15:00").getTime()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//DATE
		validRow.createCell(1).setCellValue(4097);//EVENT ID
		validRow.createCell(2).setCellValue(1);//FAIL CLASS
		validRow.createCell(3).setCellValue(21060801);//UE TYPE
		validRow.createCell(4).setCellValue(310);//MARKET NO
		validRow.createCell(5).setCellValue(21);//OPERATOR NO
		validRow.createCell(6).setCellValue(3);//CELL ID
		validRow.createCell(7).setCellValue(2000);//DURATION
		validRow.createCell(8).setCellValue(11);//CAUSE CODE
		validRow.createCell(9).setCellValue("11B");//NE VERSION
		validRow.createCell(10).setCellValue(344930000000012L);//IMSO
		validRow.createCell(11).setCellValue(4809532081614990002L);//HIER3_ID
		validRow.createCell(12).setCellValue(8226896360947470002L);//HIER32_ID
		validRow.createCell(13).setCellValue(1150444940909480002L);//HIER321_ID
		validRow.createCell(14).setCellValue("");
	}

	@Override
	void fillInvalidRow() {
		// TODO Auto-generated method stub
		
	}

	@Test
	@Override
	public void testGetsAndSets() {
		try {
			testData.setDate(new Timestamp(df.parse("11/01/2013 15:00").getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		testData.setEventId(4098);
		testData.setFailureClass(1);
		testData.setUEType(21060800);
		testData.setMarketNo(344);
		testData.setOperatorNo(930);
		testData.setCellId(4);
		testData.setDuration(1000);
		testData.setCauseCode(0);
		testData.setnEVersion("11B");
		testData.setIMSO(344930000000011L);
		testData.setHIER3_ID(4809532081614990000L);
		testData.setHIER32_ID(8226896360947470000L);
		testData.setHIER321_ID(1150444940909480000L);
		testData.setRecordId(1);
		
		assertEquals("2013-01-11 15:00:00.0", testData.getFailureDate().toString());
		assertEquals(4098, testData.getEventId());
		assertEquals(1, testData.getFailureClass());
		assertEquals(21060800, testData.getUEType());
		assertEquals(344, testData.getMarketNo());
		assertEquals(930, testData.getOperatorNo());
		assertEquals(4, testData.getCellId());
		assertEquals(1000, testData.getDuration());
		assertEquals(0, testData.getCauseCode());
		assertEquals("11B", testData.getnEVersion());
		assertEquals(344930000000011L, testData.getIMSO());
		assertEquals(4809532081614990000L, testData.getHIER3_ID());
		assertEquals(8226896360947470000L, testData.getHIER32_ID());
		assertEquals(1150444940909480000L, testData.getHIER321_ID());
		assertEquals(1, testData.getRecordId());
		
	}

	@Test
	@Override
	public void testRetrieveFromDatabase() throws ParseException {
		testData =new BaseData();
		testData.RetrieveObjectFromExcel(validRow);
		assertEquals("2013-01-11 15:00:00.0", testData.getFailureDate().toString());
		assertEquals(4097, testData.getEventId());
		assertEquals(1, testData.getFailureClass());
		assertEquals(21060801, testData.getUEType());
		assertEquals(310, testData.getMarketNo());
		assertEquals(21, testData.getOperatorNo());
		assertEquals(3, testData.getCellId());
		assertEquals(2000, testData.getDuration());
		assertEquals(11, testData.getCauseCode());
		assertEquals("11B", testData.getnEVersion());
		assertEquals(344930000000012L, testData.getIMSO());
		assertEquals(4809532081614990336L, testData.getHIER3_ID());
		assertEquals(8226896360947470336L, testData.getHIER32_ID());
		assertEquals(1150444940909480064L, testData.getHIER321_ID());
		assertEquals(0, testData.getRecordId());
	}

}
