package com.piedpiper.dataimport.tabObjects;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestMachineCodeData extends EntityTest{
	
	MachineCodeData mcd=new MachineCodeData();

	@Override
	void fillValidRow() {
		validRow.createCell(0).setCellValue(1);
		validRow.createCell(1).setCellValue(2);
		validRow.createCell(2).setCellValue("Sweden");
		validRow.createCell(3).setCellValue("Vodafone");
		validRow.createCell(4).setCellValue("Vodafone");
	}
	
	@Override
	void fillInvalidRow() {

	}
	
	@Test
	public void testGetsAndSets() {
		mcd.setMnc(1);
		mcd.setMmc(2);
		mcd.setCountry("Sweden");
		mcd.setOperator("Verizon Wireless");
		
		assertEquals(1,0.1,mcd.getMnc());
		assertEquals(2,0.1, mcd.getMmc());
		assertEquals("Sweden",mcd.getCountry());
		assertEquals("Verizon Wireless", mcd.getOperator());
	}
	
	@Override
	public void testRetrieveFromDatabase(){
		mcd.RetrieveObjectFromExcel(validRow);
		assertEquals(1, mcd.getMmc());
		assertEquals(2,mcd.getMnc());
		assertEquals("Sweden", mcd.getCountry());
		assertEquals("Vodafone", mcd.getOperator());
	}

}
