package com.piedpiper.dataimport.tabObjects;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Test;

public class TestUserEntity extends EntityTest{

	UserEntity ue;
	
	@Override
	void fillValidRow() {
		validRow.createCell(0).setCellValue(100);//TAC
		validRow.createCell(1).setCellValue("3310");//MARKET NAME
		validRow.createCell(2).setCellValue("Nokia");//MANUFACTURER
		validRow.createCell(3).setCellValue("GSM 1800");//ACCESS CAPABILITY
		validRow.createCell(4).setCellValue("3310");//MODEL
		validRow.createCell(5).setCellValue("Nokia");//VENDOR NAME
		validRow.createCell(6).setCellValue("HANDHELD");//UE TYPE
		validRow.createCell(7).setCellValue("ANDROID");//OS
		validRow.createCell(8).setCellValue("BASIC");//INPUT MODE
		validRow.createCell(9).setCellValue("");
	}
	
	@Override
	void fillInvalidRow() {

	}

	@Test
	public void testGetsAndSets() {
		ue=new UserEntity();
		ue.setTAC(22);
		ue.setMarketingName("Phone");
		ue.setManufacturer("ERICSSON");
		ue.setAccessCapability("LTE");
		ue.setModel("Phone");
		ue.setVendorName("Vodafone");
		ue.setUeType("HANDHELD");
		ue.setOperatingSys("IOS");
		ue.setInputMode("TOUCH");
		
		assertEquals(22,ue.getTAC());
		assertEquals("Phone",ue.getMarketingName());
		assertEquals("ERICSSON",ue.getManufacturer());
		assertEquals("LTE",ue.getAccessCapability());
		assertEquals("Phone",ue.getModel());
		assertEquals("Vodafone",ue.getVendorName());
		assertEquals("HANDHELD",ue.getUeType());
		assertEquals("IOS",ue.getOperatingSys());
		assertEquals("TOUCH",ue.getInputMode());
	}

	@Test
	@Override
	public void testRetrieveFromDatabase() throws ParseException {
		ue=new UserEntity();
		ue.RetrieveObjectFromExcel(validRow);
		assertEquals(100,ue.getTAC());
		assertEquals("3310",ue.getMarketingName());
		assertEquals("Nokia",ue.getManufacturer());
		assertEquals("GSM 1800",ue.getAccessCapability());
		assertEquals("3310",ue.getModel());
		assertEquals("Nokia",ue.getVendorName());
		assertEquals("HANDHELD",ue.getUeType());
		assertEquals("ANDROID",ue.getOperatingSys());
		assertEquals("BASIC",ue.getInputMode());
	}
}
