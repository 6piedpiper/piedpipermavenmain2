package com.piedpiper.dataimport.tabObjects;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Test;

public class TestEventCauseData extends EntityTest {

	EventCauseData ecd=new EventCauseData();;
	
	@Override
	void fillValidRow() {
		validRow.createCell(0).setCellValue(16);//CAUSE CODE
		validRow.createCell(1).setCellValue(4097);//EVENT ID
		validRow.createCell(2).setCellValue("RRC CONN SETUP-DSP RESTART");//DESCRIPTION
		validRow.createCell(3).setCellValue(22);
	}

	@Override
	void fillInvalidRow() {
		// TODO Auto-generated method stub
		
	}

	@Test
	@Override
	public void testGetsAndSets() {
		ecd.setCauseCode(5);
		ecd.setEventId(4097);
		ecd.setDescription("RRC CONN SETUP-LACK OF RESOURCES");
		
		assertEquals(5, ecd.getCauseCode());
		assertEquals(4097, ecd.getEventId());
		assertEquals("RRC CONN SETUP-LACK OF RESOURCES", ecd.getDescription());
	}

	@Test
	@Override
	public void testRetrieveFromDatabase() throws ParseException {
		ecd=new EventCauseData();
		ecd.RetrieveObjectFromExcel(validRow);
		assertEquals(16, ecd.getCauseCode());
		assertEquals(4097, ecd.getEventId());
		assertEquals("RRC CONN SETUP-DSP RESTART", ecd.getDescription());
	}
	
	@Test
	public void testEventCausePK(){
		EventCausePK pk=new EventCausePK();
		pk.setCauseCode(5);
		pk.setEventId(4097);
		
		assertEquals(5, pk.getCauseCode());
		assertEquals(4097, pk.getEventId());
	}

}
