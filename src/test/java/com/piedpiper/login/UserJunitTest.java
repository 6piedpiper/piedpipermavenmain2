package com.piedpiper.login;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.piedpiper.users.User;
import com.piedpiper.users.UserRole;

public class UserJunitTest {
	private User user1;
	int empId = 1;
	String userName = "admin";
	String password = "password";
	UserRole userRole = UserRole.SYSTEM_ADMINISTRATOR;

	@Before
	public void setup() {
		user1 = new User();
	}

	@Test
	public void testSetAndGetEmpId() {
		user1.setEmpId(empId);
		assertEquals(1, user1.getEmpId());
	}

	@Test
	public void testSetAndGetUserName() {
		user1.setUserName(userName);
		assertEquals("admin", user1.getUserName());
	}

	@Test
	public void testSetAndGetPword() {
		user1.setpWord(password);
		assertEquals("password", user1.getpWord());
	}

	@Test
	public void testSetAndGetUserRole() {
		user1.setUserRole(userRole);
		assertEquals("SYSTEM_ADMINISTRATOR", user1.getUserRole());
	}

	@Test
	public void testToString() {
		user1.setEmpId(1);
		user1.setUserName(userName);
		user1.setpWord(password);
		assertEquals("User [id= 1, User Name= admin, hashCode= 1]", user1.toString());
	}

	@Test
	public void testHashode() {
		user1.setEmpId(1);
		assertEquals(1, user1.hashCode());
	}

	@Test
	public void testEquals() {
		user1.setEmpId(1);
		user1.setUserName(userName);
		user1.setpWord(password);
		User user2 = new User();
		user2.setEmpId(1);
		user2.setUserName(userName);
		user2.setpWord(password);
		assertTrue(user1.equals(user2));
	}

}
