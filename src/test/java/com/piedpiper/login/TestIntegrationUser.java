package com.piedpiper.login;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.piedpiper.data.UserDAO;
import com.piedpiper.dataimport.RestApplication;
import com.piedpiper.rest.UserWebServices;
import com.piedpiper.users.User;
import com.piedpiper.users.UserRole;
import com.piedpiper.utility.UtilsDAO;

// @FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Arquillian.class)
public class TestIntegrationUser {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(JavaArchive.class, "Test.jar")
				.addClasses(UserDAO.class, User.class, RestApplication.class, UserWebServices.class, UtilsDAO.class)
				// .addPackage(EventCause.class.getPackage())
				// .addPackage(EventCauseDAO.class.getPackage())
				// this line will pick up the production db
				.addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
		// Change to add as a Web Resource....Line Above

	}

	private static boolean setUpIsDone = false;

	@EJB
	private UserWebServices userWS;

	@EJB
	private UserDAO userDAO;

	@EJB
	private UtilsDAO utilsDAO;

	@Before
	public void setUp() {
		// this function means that we start with an empty table
		// And add one wine
		// it should be possible to test with an in memory db for efficiency
		utilsDAO.deleteTable();
		User user1 = new User();
		user1.setEmpId(1);
		user1.setUserName("Arquillian");
		user1.setpWord("test");
		user1.setUserRole(UserRole.SUPPORT_ENGINEER);

		userDAO.add(user1);
	}

	@Test
	public void testGetAllUsers() {
		List<User> userList = userDAO.getAllUsers();
		assertEquals("Data fetch = data persisted", userList.size(), 1);
	}

	@Test
	public void testGetUserByName() {
		List<User> userList = userDAO.getUsersByName("Arquillian");
		assertTrue(userList.get(0).getUserName().equals("Arquillian"));
	}

	@Test
	public void testGetUserById() {
		User user = userDAO.getUserById(1);
		assertTrue(user.getUserName().equals("Arquillian"));
	}

	@Test
	public void testSaveMethod() {
		User user2 = new User();
		user2.setEmpId(2);
		user2.setUserName("Test 2");
		user2.setpWord("test2");
		user2.setUserRole(UserRole.SUPPORT_ENGINEER);

		userDAO.add(user2);
		List<User> userList = userDAO.getAllUsers();
		assertEquals("Data fetch = data persisted", userList.size(), 2);

	}

	@Test
	public void testUpdate() {
		User user3 = new User();
		user3.setEmpId(2);
		user3.setUserName("Test 2");
		user3.setpWord("test2");
		user3.setUserRole(UserRole.SUPPORT_ENGINEER);

		user3.setUserName("Peter Andre");
		userDAO.update(user3);

		assertTrue(user3.getUserName().equals("Peter Andre"));

	}

	@Test
	public void testDelete() {
		userDAO.delete(1);
		List<User> userList = userDAO.getAllUsers();
		assertEquals("User Deleted", userList.size(), 0);
	}

}
